/* main.vala
 *
 * Copyright (C) 2016 Miceal Gallagher
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Gtk;

int main (string[] args) {
    Gtk.init (ref args);

    try{
			var builder = new Builder();
			builder.add_from_file("./ui/WizardSkeleton.glade");
			builder.connect_signals(null);

			var window = builder.get_object("window") as Window;
			window.show_all();

			Gtk.main();

    } catch (Error e) {
    	stderr.printf ("Couldn't load ui %s\n", e.message);
    	return 1;
    }

    return 0;
}
